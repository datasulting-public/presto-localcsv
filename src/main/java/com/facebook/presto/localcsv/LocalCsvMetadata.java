/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.trino.localcsv;

//import io.trino.spi.*;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import io.trino.spi.connector.ColumnHandle;
import io.trino.spi.connector.ColumnMetadata;
import io.trino.spi.connector.ConnectorMetadata;
import io.trino.spi.connector.ConnectorSession;
import io.trino.spi.connector.ConnectorTableHandle;
import io.trino.spi.connector.ConnectorTableLayout;
import io.trino.spi.connector.ConnectorTableLayoutHandle;
import io.trino.spi.connector.ConnectorTableLayoutResult;
import io.trino.spi.connector.ConnectorTableMetadata;
import io.trino.spi.connector.Constraint;
import io.trino.spi.connector.SchemaTableName;
import io.trino.spi.connector.SchemaTablePrefix;
import io.trino.spi.type.VarcharType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class LocalCsvMetadata
                                implements ConnectorMetadata
{
    private LocalCsvConfig config;
    private LocalCsvUtils utils;

    @Inject
    public LocalCsvMetadata(LocalCsvConfig config, LocalCsvUtils utils)
    {
        this.config = config;
        this.utils = utils;
    }

    @Override
    public List<String> listSchemaNames(ConnectorSession session)
    {
        try {
            return Files.list(config.getCsvDir().toPath()).filter(Files::isDirectory).map(p -> p.getFileName().toString()).collect(Collectors.toList());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ConnectorTableHandle getTableHandle(ConnectorSession session, SchemaTableName tableName)
    {
        Path tablePath = config.getCsvDir().toPath().resolve(tableName.getSchemaName());
        if (!Files.exists(tablePath) || !Files.isDirectory(tablePath)) {
            return null;
        }
        Path csvPath = tablePath.resolve(tableName.getTableName() + ".csv");
        if (!Files.exists(csvPath)) {
            return null;
        }
        return new LocalCsvTableHandle(tableName.getSchemaName(), tableName.getTableName());
    }

    @Override
    public List<ConnectorTableLayoutResult> getTableLayouts(ConnectorSession session, ConnectorTableHandle table, Constraint constraint, Optional<Set<ColumnHandle>> desiredColumns)
    {
        LocalCsvTableHandle tableHandle = (LocalCsvTableHandle) table;
        ConnectorTableLayout layout = new ConnectorTableLayout(new LocalCsvTableLayoutHandle(tableHandle));
        return ImmutableList.of(new ConnectorTableLayoutResult(layout, constraint.getSummary()));
    }

    @Override
    public ConnectorTableLayout getTableLayout(ConnectorSession session, ConnectorTableLayoutHandle handle)
    {
        return new ConnectorTableLayout(handle);
    }

    @Override
    public ConnectorTableMetadata getTableMetadata(ConnectorSession session, ConnectorTableHandle table)
    {
        LocalCsvTableHandle handle = (LocalCsvTableHandle) table;
        List<String> columns = utils.tableColumns(handle.getSchemaTableName());
        List<ColumnMetadata> metadata = columns.stream().map(c -> new ColumnMetadata(c, VarcharType.VARCHAR)).collect(Collectors.toList());
        return new ConnectorTableMetadata(handle.getSchemaTableName(), metadata);
    }

    @Override
    public List<SchemaTableName> listTables(ConnectorSession session, Optional<String> schemaName)
    {
        ImmutableList.Builder<String> schemaListBuilder = ImmutableList.builder();
        ImmutableList.Builder<SchemaTableName> tableNamesBuilder = ImmutableList.builder();
        if (schemaName.isPresent()) {
            schemaListBuilder.add(schemaName.get());
        }
        else {
            schemaListBuilder.addAll(listSchemaNames(session));
        }
        ImmutableList<String> schemas = schemaListBuilder.build();
        for (String schema : schemas) {
            tableNamesBuilder.addAll(listTables(schema));
        }
        return tableNamesBuilder.build();
    }

    @Override
    public Map<String, ColumnHandle> getColumnHandles(ConnectorSession session, ConnectorTableHandle tableHandle)
    {
        ImmutableMap.Builder<String, ColumnHandle> builder = ImmutableMap.builder();
        LocalCsvTableHandle handle = (LocalCsvTableHandle) tableHandle;
        List<String> columns = utils.tableColumns(handle.getSchemaTableName());
        int idx = 0;
        for (String colunm : columns) {
            builder.put(colunm, new LocalCsvColumnHandle(colunm, VarcharType.VARCHAR, idx));
            idx++;
        }
        return builder.build();
    }

    @Override
    public ColumnMetadata getColumnMetadata(ConnectorSession session, ConnectorTableHandle tableHandle, ColumnHandle columnHandle)
    {
        LocalCsvColumnHandle handle = (LocalCsvColumnHandle) columnHandle;
        return new ColumnMetadata(handle.getColumnName(), handle.getColumnType());
    }

    @Override
    public Map<SchemaTableName, List<ColumnMetadata>> listTableColumns(ConnectorSession session, SchemaTablePrefix prefix)
    {
        /*List<SchemaTableName> list = new ArrayList<>();
        if (prefix.getTableName() != null) {
            list.add(new SchemaTableName(prefix.getSchemaName(), prefix.getTableName()));
        }
        else {
            list.addAll(listTables(prefix.getSchemaName()));
        }
        ImmutableMap.Builder<SchemaTableName, List<ColumnMetadata>> builder = ImmutableMap.builder();
        for (SchemaTableName name : list) {
            List<String> columns = utils.tableColumns(name);
            List<ColumnMetadata> metadata = columns.stream().map(c -> new ColumnMetadata(c, VarcharType.VARCHAR)).collect(Collectors.toList());
            builder.put(name, metadata);
        }
        return builder.build();*/
        requireNonNull(prefix, "prefix is null");
        ImmutableMap.Builder<SchemaTableName, List<ColumnMetadata>> builder = ImmutableMap.builder();

        // NOTE: prefix.getTableName or prefix.getSchemaName can be null
        List<SchemaTableName> tableNames;
        if (prefix.getSchema().isPresent() && prefix.getTable().isPresent()) {
            tableNames = ImmutableList.of(new SchemaTableName(prefix.getSchema().get(), prefix.getTable().get()));
        }
        else {
            tableNames = listTables(session, Optional.empty());
        }

        for (SchemaTableName tableName : tableNames) {
            List<String> columns = utils.tableColumns(tableName);
            List<ColumnMetadata> metadata = columns.stream().map(c -> new ColumnMetadata(c, VarcharType.VARCHAR)).collect(Collectors.toList());
            builder.put(tableName, metadata);
        }

        return builder.build();
    }

    private List<SchemaTableName> listTables(String schemaName)
    {
        try {
            return Files.list(config.getCsvDir().toPath().resolve(schemaName))
                    .map(p -> p.getFileName().toString().replaceAll("\\.csv$", ""))
                    .map(tableName -> new SchemaTableName(schemaName, tableName)).collect(Collectors.toList());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
